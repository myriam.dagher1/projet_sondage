<?php

require_once 'session.php';
require_once 'class_php.php';

if(isset($_POST['btn'])){ //Si le bouton est appuyés alors on continue

    $pseudo1 = $_POST['pseudo']; // On récupère les données du formulaire

    $database1 = new DB(); // On appelle notre class DB 
    $db1 = $database1->connect(); // On appelle notre fonction connect
    
    $addPseudo = new user($db1); // On appelle notre classe pseudo on insérant en paramètre la connexion à la BDD
    $addPseudo2 = $addPseudo->addpseudo($pseudo1); // On appelle notre fonction addpseudo avec en paramètre le pseudo récupérer dans le formulaire
    $_SESSION['pseudo'] = $pseudo1;
    // echo  $_SESSION['pseudo']

    header('Location:sondage.php');
}

// $database = new DB(); // On appelle notre class DB 
// $db = $database->connect(); // On appelle notre fonction connect
// $getPseudo1 = new pseudo($db);
// $getpseudo = $getPseudo1->getId($_SESSION['pseudo']);



?>
<!DOCTYPE html> 
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="identifiez-vous.css">
    <link rel="stylesheet" href="style.css">
    <title>Identifiez-vous</title>
</head>

<body>
    <div id="form1">
        <h1 id="welcome-message">Bienvenue, choisissez un identifiant</h1>

        <form id="survey-form" action="" method="post">
            
            <input id="textarea" type="text" name="pseudo" id="pseudo">
            <input id="submit" type="submit" name="btn" value="Me connecter">

        </form>
    </div>

</body>

</html>