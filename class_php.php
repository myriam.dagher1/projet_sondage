<?php


//Class de connexion à la BDD 
class DB
{

    //Création des attributs privé, ils ne seront accéssible qu'a partir de cette class
    private $host = 'localhost';
    private $db_name = 'sondage';
    private $username = 'sondage';
    private $password = 'pass_sondage';
    private $conn;

    //création d'une fonction public celle-ci sera accécible à partir d'une autre class
    public function connect()
    {
        $this->conn = null;
        try {
            $this->conn = new PDO('mysql:host=' . $this->host . ';dbname=' . $this->db_name, $this->username, $this->password);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo 'Connection Error: ' . $e->getMessage();
        }

        return $this->conn;
    }
}


// Class de création de pseudo

class user
{

    public $pseudo; //attribut public
    private $conn; // attribut privés

    public function __construct($db)
    { // Le construct sert à ce qu'a chaque fois que la class est appelée alors cette fonction est exécuté
        $this->conn = $db;
    }


    /**
     * Undocumented function
     *
     * @param string $pseudo
     * @return void
     */
    public function addpseudo(string $pseudo)
    { //fonction public, accessible depuis une autre class

        // Préparation de la requête d'insertion
        $stmt = $this->conn->prepare("INSERT INTO utilisateurs (pseudo) VALUES (:pseudo)");
        $stmt->bindParam(':pseudo', $pseudo);
        $stmt->execute();
        $_SESSION['pseudo'] = $pseudo;
        // var_dump($pseudo);
    }

    /**
     * Undocumented function
     *
     * @param string $pseudo
     * 
     */
    public function getId(string $pseudo)
    { // récupérer les pseudo en BDD 
        $query = $this->conn->prepare("SELECT id FROM utilisateurs WHERE pseudo = :pseudo ORDER BY id DESC");
        $query->bindParam(':pseudo', $pseudo);
        $query->execute();
        $id = $query->fetch();
        // echo $id['id'];
        $_SESSION['id'] = $id['id'];
        // $utilisateur = $query->fetchAll();
        // print_r($utilisateur);
    }

    public function vote(int $iduser, int $idsondage, int $reponse)
    {

        $vote = $this->conn->prepare("INSERT INTO vote (utilisateur_id, sondage_id, reponse_id)
        VALUES (:iduser, :idsondage, :idreponse)");

        $vote->bindParam(':iduser', $iduser);
        $vote->bindParam(':idsondage', $idsondage);
        $vote->bindParam(':idreponse', $reponse);
        $vote->execute();
    }
}

class sondage
{

    public $conn;
    public $question;
    public $reponse;
    public $resultat;

    public function __construct($db)
    { // Le construct sert à ce qu'a chaque fois que la class est appelée alors cette fonction est exécuté
        $this->conn = $db;
    }



    /**
     * Ajouter un sondage
     *
     * @param string $question
     * @param integer $iduser
     * @return void
     */
    public function addquestion(string $question, int $iduser)
    {


        function generateKey()
        {
            $characters = '123456789';
            $charactersLength = strlen($characters);
            $randomString = $characters[rand(1, $charactersLength - 1)];
            for ($i = 0; $i < 10; $i++) {
                
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }

        $idsondage = generateKey();
        $_SESSION['generate_key'] = $idsondage;


        // Préparation de la requête d'insertion
        $stmt = $this->conn->prepare("INSERT INTO sondages (id, utilisateur_id, question) VALUES (:idsondage, :iduser, :question)");
        $stmt->bindParam(':idsondage', $idsondage);
        $stmt->bindParam(':iduser', $iduser);
        $stmt->bindParam(':question', $question);
        $stmt->execute();

        return $idsondage;
    }

    /**
     * Afficher le sondage 
     *
     * @param string $idsondages
     * @return array
     */

    public function getsondage(string $idsondages): array
    {

        $sth2 = $this->conn->prepare("SELECT sondages.question, choix.reponse, choix.id FROM sondages 
        JOIN choix on choix.sondage_id = sondages.id WHERE sondages.id = :idsondage");

        $sth2->bindParam(':idsondage', $idsondages);
        $sth2->execute();

        $_SESSION['key'] = $idsondages;
        $question1 = $sth2->fetchAll();
        return $question1;
    }

    /**
     * ajouter des choix de réponses 
     *
     * @param string $reponse
     * @return void
     */
    public function addreponse(string $reponse)
    {

        $sth = $this->conn->prepare("SELECT sondages.id FROM sondages JOIN utilisateurs on utilisateurs.id = sondages.utilisateur_id WHERE utilisateurs.id = :id ORDER BY date_creation DESC");
        $sth->bindParam(':id', $_SESSION['id']);
        $sth->execute();
        $idsondage1 = $sth->fetch();
        $idsondage = $idsondage1['id'];


        $stmt1 = $this->conn->prepare("INSERT INTO choix (sondage_id, reponse) VALUES (:idsondage, :reponse)");
        $stmt1->bindParam(':idsondage', $idsondage);
        $stmt1->bindParam(':reponse', $reponse);
        $stmt1->execute();
    }



    /**
     * Undocumented function
     *
     * @param integer $idsondage
     * @return array
     */
    public function AfficherResultat(int $idsondage): array
    {

        $Afficher = $this->conn->prepare("SELECT choix.reponse, sondages.question from vote JOIN choix 
        on choix.id = vote.reponse_id
        JOIN sondages on sondages.id = vote.sondage_id
        WHERE choix.sondage_id = :idsondage");
        $Afficher->bindParam(':idsondage', $idsondage);
        $Afficher->execute();
        $Afficher1 = $Afficher->fetchAll();
        return $Afficher1;
    }
}
