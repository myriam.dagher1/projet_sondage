<?php

require_once 'session.php';
require_once 'class_php.php';

$pseudo = $_SESSION['pseudo'];

$database = new DB(); // On appelle notre class DB 
$db = $database->connect(); // On appelle notre fonction connect

$getPseudo1 = new user($db);
$getpseudo = $getPseudo1->getId($_SESSION['pseudo']);

// echo $_SESSION['id'];

$idUser = (int)$_SESSION['id'];



if (isset($_POST['btn2'])) { //si on appuie sur le bouton alors on continue

    if (!empty($_POST['titre']) && !empty($_POST['choix1']) && !empty($_POST['choix2'])) { //si les champs de formulaire ne sont pas vides alors on continue


        $question = $_POST['titre'];
        $choix1 = $_POST['choix1'];
        $choix2 = $_POST['choix2'];
        $choix3 = $_POST['choix3'];
        $choix4 = $_POST['choix4'];


        $database1 = new DB(); // On instancit notre class DB 
        $db1 = $database1->connect();

        $addQuestion = new sondage($db1);
        $addQuestion1 = $addQuestion->addquestion($question, $idUser);

        $addAnswer = new sondage($db1);
        $addAnswer1 = $addAnswer->addreponse($choix1);
        $addAnswer2 = $addAnswer->addreponse($choix2);


        if (!empty($_POST['choix3'])) {

            $addAnswer3 = new sondage($db1);
            $addAnswer8 = $addAnswer3->addreponse($choix3);

            if (!empty($_POST['choix4'])) {

                $addAnswer4 = new sondage($db1);
                $addAnswer5 = $addAnswer4->addreponse($choix4);
            }

            // $addAnswer2 = $addAnswer3->addreponse($choix4);
        }
    }
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="index.css">
    <title>Créez un sondage ou insérer la clé d'un sondage </title>
</head>


<!-- <form action="" method="post">

        <input type="text" name="titre" id="" placeholder="titre du vote">

        <input type="text" name="choix1" id="" placeholder="choix 1">
        <input type="text" name="choix2" id="" placeholder="choix 2">
        <input type="text" name="choix3" id="" placeholder="choix 3">
        <input type="text" name="choix4" id="" placeholder="choix 4">

        <input type="submit" name="btn2" value="Créer le vote">

    </form> -->

<body>
    <section class="partieleft">
        <h1>Créer un vote !</h1>
        <div class="form-group">
            <form method="post">

                <input type="text" placeholder="Titre du vote" id="voteTitle" name="titre">
        </div>
        <div class="form-group">
            <input type="text" placeholder="Description" id="Description" name="Description">
        </div>
        <div class="form-group">
            <input type="text" placeholder="Option1" id="voteOption1" name="choix1">
        </div>

        <div class="form-group">
            <input type="text" placeholder="Option2" id="voteOption2" name="choix2">
        </div>
        <div class="form-group">
            <input type="text" placeholder="Option3 (optionnel)" id="voteOption3" name="choix3">
        </div>
        <div class="form-group">
            <input type="text" placeholder="Option4 (optionnel)" id="voteOption4" name="choix4">
        </div>
        <input type="submit" name="btn2" class="btn btn-primary" value="créer le formulaire">

        </form>
    </section>


    <section class="partieright">

        <h1>Participer à un vote !</h1>

        <form action="reponse_sondage.php" method="post">

            <div class="form-group">
                <h2><label for="key">Clé du sondage</label></h2>
                <input type="text" placeholder="Clé du vote" name="key" id="voteClé">
            </div>
            <input type="submit" class="btn btn-primary" name="btn3" value="Soumettre">

        </form>

    </section>

    <?php
    if (isset($_SESSION['generate_key'])) {

    ?>
        <h3 class="clévote">La clé pour voté est:</br></br><?php echo $_SESSION['generate_key'];
                                                        } ?></h3>

</body>

</html>