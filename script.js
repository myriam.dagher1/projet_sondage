fetch('afficher_sondage.php')
  .then(function (response) {
    return response.json(); // ici on traite la réponse JSON et on la convertit en objet javascript : equivalent à JSON.parse()
  })
  .then(function (obj) {
    console.log(obj)

    let pollResults = document.getElementById("response");//on récupére la div par l'id 

    pollResults.innerHTML = "";//on l'initialise à 0

    let totalCount = obj.length; // on compte les objets contenu dans le JSON

    let reponseCounts = {};// on crée un tableau pour compter les réponses

    obj.forEach(function (question) { // on crée un foreach pour parcourir notre JSON

      let reponse = question.reponse; // on recupère les réponses de notre objet question

      if (!reponseCounts[reponse]) { // on lui impose une condition qui verifie si la reponse est similaire 
        reponseCounts[reponse] = 0;// si la reponse n'est pas similaire alors c'est égale à 0
      }
      reponseCounts[reponse]++; // si il existe alors on l'ajoute 
    });

    pollResults.innerHTML += "<h2>Les résultats du sondages :<br><br>" + obj[0].question + "?</h2><br>";
    for (let reponse in reponseCounts) {
      let percentage = (reponseCounts[reponse] / totalCount) * 100;// il calcule le porcentage d'une réponse
      pollResults.innerHTML += "<h3>" + reponse + " : " + percentage.toFixed(2) + "%</h3>";
    }

    let labels = Object.keys(reponseCounts);
    let data = Object.values(reponseCounts);

    let ctx = document.getElementById("chart").getContext("2d");
    let chart = new Chart(ctx, {
      type: "bar",
      data: {
        labels: labels,
        datasets: [
          {
            barPercentage: 1.0,
            barThickness: 10,
            // data: [10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
            label: "Réponses",
            data: data,
            backgroundColor: ["#3e95cd", "#8e5ea2", "#3cba9f", "#e8c3b9", "#c45850"]
          }
        ]
      },
      options: {

        indexAxis: 'y',
        scales: {
          x: {
            stacked:true
          },
          y: {
            stacked: true
          }
        }
      }
    });
  });


