<?php

 // suppression des données de session sur le serveur
 session_start(); 

 session_unset(); 
 session_destroy();

 // suppression du cookie de session sur le poste client
 setcookie(session_name(), '', strtotime('-1 day'));
 header("location:index.php");

