<?php

require_once 'class_php.php';
require_once 'session.php';


$database = new DB(); // On appelle notre class DB 
$db = $database->connect();

//afficher les vote comptabilisé
$resultat = new sondage($db);
$resultat1 = $resultat->AfficherResultat((int)$_SESSION['key']);

// On indique au client que l'on va lui renvoyer des données JSON
// Pour cela on précise l'entête de réponse HTTP Content-Type, grâce à la fonction PHP header : https://www.php.net/manual/fr/function.header
header("Content-Type: application/json");

if(empty($resultat1)){

    echo json_encode(["message" => "No results found"]);

}else{

    echo json_encode($resultat1);
    
}

