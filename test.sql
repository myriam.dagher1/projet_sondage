use sondage;

SET @user_pseudo='zahra';

INSERT INTO
    utilisateurs (pseudo)
VALUES @user_pseudo,
SET
    @user_id = LAST_INSERT_ID();

INSERT INTO
    sondages (utilisateur_id, question)
VALUES (1, "TEST1"), (1, "TEST2"), (1, "TEST3"), (2, "TEST1"), (2, "TEST2"), (2, "TEST3");

INSERT INTO
    reponses_possibles (sondage_id, reponse)
VALUES (7, "reponse1"), (7, "reponse2"), (7, "reponse3"), (8, "reponse1"), (8, "reponse2"), (8, "reponse3"), (9, "reponse1"), (9, "reponse2"), (9, "reponse3"), (10, "reponse1"), (10, "reponse2"), (10, "reponse3"), (11, "reponse1"), (11, "reponse2"), (11, "reponse3"), (12, "reponse1"), (12, "reponse2"), (12, "reponse3");

INSERT INTO
    reponses (
        utilisateur_id,
        sondage_id,
        reponse_id
    )
VALUES (1, 7, 1), (1, 8, 4), (1, 9, 7), (2, 10, 10), (2, 11, 13), (2, 12, 16);