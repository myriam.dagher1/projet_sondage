<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="script.js" defer></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>
    <link rel="stylesheet" href="style.css">
    <title>Résultat du sondage</title>
    <script></script>
</head>

<body>
    <div id="lien">

        <a href="deconnexion.php">Déconnexion</a>
    </div>
    <div id="response"></div>

    <canvas id="chart"></canvas>

</body>

</html>