create database IF NOT EXISTS sondage;

CREATE USER IF NOT EXISTS 'sondage'@'localhost' IDENTIFIED BY 'pass_sondage';

GRANT ALL PRIVILEGES ON sondage.* TO 'sondage'@'localhost';

FLUSH PRIVILEGES;

use sondage;

CREATE TABLE utilisateurs (

  id INT AUTO_INCREMENT PRIMARY KEY,
  pseudo VARCHAR(255) NOT NULL

);

CREATE TABLE sondages (

  id VARCHAR(15) PRIMARY KEY,
  utilisateur_id INT NOT NULL,
  question VARCHAR(255) NOT NULL,
  date_creation DATETIME NOT NULL DEFAULT (CURRENT_TIMESTAMP),
  FOREIGN KEY (utilisateur_id) REFERENCES utilisateurs(id)

);

CREATE TABLE choix (

  id INT AUTO_INCREMENT PRIMARY KEY,
  sondage_id VARCHAR(15) NOT NULL,
  reponse VARCHAR(255) NOT NULL,
  FOREIGN KEY (sondage_id) REFERENCES sondages(id)
);

CREATE TABLE vote (

  id INT AUTO_INCREMENT PRIMARY KEY,
  utilisateur_id INT NOT NULL,
  sondage_id VARCHAR(15) NOT NULL,
  reponse_id INT NOT NULL,
  date_creation DATETIME NOT NULL DEFAULT (CURRENT_TIMESTAMP),
  FOREIGN KEY (utilisateur_id) REFERENCES utilisateurs(id),
  FOREIGN KEY (sondage_id) REFERENCES sondages(id),
  FOREIGN KEY (reponse_id) REFERENCES choix(id)
  
);
